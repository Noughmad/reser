#[macro_use]
extern crate failure;
extern crate serde_any;
extern crate serde_value;

#[macro_use]
extern crate structopt;

use structopt::StructOpt;

use std::path::PathBuf;
use std::io::{self, Read};
use std::fs::File;

use serde_any::Format;
use serde_value::Value;
use failure::Error;

#[derive(StructOpt, Clone, Debug)]
struct Opt {
    #[structopt(short = "i", long = "input", parse(from_os_str))]
    input: Option<PathBuf>,
    #[structopt(short = "o", long = "output", parse(from_os_str))]
    output: Option<PathBuf>,
    #[structopt(short = "f", long = "format")]
    output_format: Option<Format>,
    #[structopt(short = "n", long = "input-format")]
    input_format: Option<Format>,
    #[structopt(short = "p", long = "pretty")]
    pretty: bool,
}

fn path_is_not_dash(path: &PathBuf) -> bool {
    path.as_os_str() != "-"
}

impl Opt {
    fn deserialize(&self) -> Result<Value, Error> {
        let input = self.input.clone().filter(path_is_not_dash);
        let value = match (input, self.input_format) {
            (Some(i), None) => serde_any::from_file(&i)?,
            (Some(i), Some(f)) => {
                let file = File::open(i)?;
                serde_any::from_reader(file, f)?
            }
            (None, Some(f)) => serde_any::from_reader(io::stdin(), f)?,
            (None, None) => {
                let mut data = vec![];
                io::stdin().read_to_end(&mut data)?;
                serde_any::from_slice_any(&data)?
            }
        };
        Ok(value)
    }

    fn serialize(&self, value: &Value) -> Result<(), Error> {
        let output = self.output.clone().filter(path_is_not_dash);
        match (&output, self.output_format, self.pretty) {
            (Some(o), None, false) => serde_any::to_file(&o, value)?,
            (Some(o), None, true) => serde_any::to_file_pretty(&o, value)?,
            (Some(o), Some(f), false) => {
                let file = File::create(o)?;
                serde_any::to_writer(file, value, f)?
            }
            (Some(o), Some(f), true) => {
                let file = File::create(o)?;
                serde_any::to_writer_pretty(file, value, f)?
            }
            (None, Some(f), false) => serde_any::to_writer(io::stdout(), value, f)?,
            (None, Some(f), true) => serde_any::to_writer_pretty(io::stdout(), value, f)?,
            (None, None, _) => bail!("Either --format or --output must be specified"),
        };

        if output.is_none() {
            println!("");
        }

        Ok(())
    }
}

//
// A program that will "transcode" a House object between different
// serialization formats.
//
// The user can specify `input` and `output` paths with any supported file
// extension, and the program will use it to choose the deserialization and
// serialization format.
//
// For example, `transcode -i house.toml -o house.json` will convert it from
// TOML to JSON.
//

fn main() -> Result<(), failure::Error> {
    let opt = Opt::from_args();

    let value = opt.deserialize()?;

    opt.serialize(&value)?;

    Ok(())
}
