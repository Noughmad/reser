extern crate assert_cli;

use assert_cli::Assert;

fn json() -> Assert {
    Assert::main_binary().with_args(&["--input-format", "json", "--format", "json"])
}

#[test]
fn fails_without_required_arguments() {
    Assert::main_binary().fails().unwrap()
}

#[test]
fn json_fails_with_empty_input() {
    json().fails().unwrap();
}

#[test]
fn succeeds_with_json_input() {
    json().stdin(r#"{"a":"b"}"#).unwrap()
}

#[test]
fn fails_with_invalid_input() {
    json().stdin(r#"{"a": "b"oeae}"#).fails().unwrap()
}

#[test]
fn simple_json_preserve() {
    let data = r#"{"a":"b"}"#;
    json().stdin(data).stdout().is(data).unwrap()
}
